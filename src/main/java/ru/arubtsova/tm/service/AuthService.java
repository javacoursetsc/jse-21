package ru.arubtsova.tm.service;

import ru.arubtsova.tm.api.service.IAuthService;
import ru.arubtsova.tm.api.service.IUserService;
import ru.arubtsova.tm.exception.authorization.AccessDeniedException;
import ru.arubtsova.tm.exception.empty.EmptyLoginException;
import ru.arubtsova.tm.exception.empty.EmptyPasswordException;
import ru.arubtsova.tm.model.User;
import ru.arubtsova.tm.util.HashUtil;

import java.util.Optional;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public Optional<User> getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final Optional<User> user = userService.findByLogin(login);
        //if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.get().getPasswordHash())) throw new AccessDeniedException();
        userId = user.get().getId();
    }

    @Override
    public void register(final String login, final String password, final String email) {
        userService.create(login, password, email);
    }

}
