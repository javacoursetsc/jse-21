package ru.arubtsova.tm.command.project;

import ru.arubtsova.tm.command.AbstractProjectCommand;
import ru.arubtsova.tm.exception.entity.ProjectNotFoundException;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Override
    public String description() {
        return "update a project by id.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Project:");
        System.out.println("Enter Project Id:");
        final String id = TerminalUtil.nextLine();
        final Optional<Project> project = serviceLocator.getProjectService().findById(userId, id);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Enter New Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter New Project Description:");
        final String description = TerminalUtil.nextLine();
        final Optional<Project> projectUpdate = serviceLocator.getProjectService().updateById(userId, id, name, description);
        Optional.ofNullable(projectUpdate).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Project was successfully updated");
    }

}
