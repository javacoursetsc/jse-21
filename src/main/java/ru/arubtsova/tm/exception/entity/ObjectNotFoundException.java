package ru.arubtsova.tm.exception.entity;

import ru.arubtsova.tm.exception.AbstractException;

public class ObjectNotFoundException extends AbstractException {

    public ObjectNotFoundException() {
        super("Error! Object not found...");
    }

}
