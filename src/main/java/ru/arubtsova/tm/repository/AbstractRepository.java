package ru.arubtsova.tm.repository;

import ru.arubtsova.tm.api.IRepository;
import ru.arubtsova.tm.model.AbstractEntity;

import java.util.*;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected Map<String, E> map = new LinkedHashMap<>();

    @Override
    public List<E> findAll() {
        return new ArrayList<>(map.values());
    }

    @Override
    public void add(final E entity) {
        map.put(entity.getId(), entity);
    }

    @Override
    public Optional<E> findById(final String id) {
        return Optional.ofNullable(map.get(id));
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public void remove(final E entity) {
        map.remove(entity.getId());
    }

    @Override
    public E removeById(final String id) {
        final Optional<E> entity = findById(id);
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

}
