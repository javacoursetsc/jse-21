package ru.arubtsova.tm.api.entity;

import ru.arubtsova.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
