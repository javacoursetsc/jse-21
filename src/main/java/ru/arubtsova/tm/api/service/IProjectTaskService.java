package ru.arubtsova.tm.api.service;

import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    Task bindTaskToProject(String userId, String taskId, String projectId);

    Task unbindTaskFromProject(String userId, String taskId);

    Project removeProjectWithTasksById(String userId, String projectId);

}
