package ru.arubtsova.tm.api.service;

public interface ILogService {

    void debug(String message);

    void info(String message);

    void command(String message);

    void error(Exception e);

}
