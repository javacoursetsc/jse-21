package ru.arubtsova.tm.api.repository;

import ru.arubtsova.tm.api.IBusinessRepository;
import ru.arubtsova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

    void removeAllByProjectId(String userId, String projectId);

    Task bindTaskToProject(String userId, String taskId, String projectId);

    Task unbindTaskFromProject(String userId, String projectId);

}
